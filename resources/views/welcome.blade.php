<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href=" {{asset('css/style.css')}}" rel="stylesheet">
        <title>Cactu na Caneca</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
          <div class="content">

          <div class="text-center banner-header">
                <img class="banner-content" src="https://via.placeholder.com/150">
          </div>

          <nav class="navbar navbar-expand-lg navbar-dark menu-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="#" data-toggle="modal" data-target="#loginModal"> Login <span class="sr-only">(current)</span></a>
                </li>
              </ul>
              <form class="form-inline my-2 my-lg-0" action="" method="GET">
                  <input class="form-control mr-sm-2 search-input-header" type="text" placeholder="Pesquisar Postagem" name="namePost">
                  <button type="submit" class="form-control text-center my-2 my-sm-0"><i class="fa fa-search"> Procurar </i></button>
                </form>
            </div>
          </nav>

          <div class="container post-div">
            <div>
              <h1>Tentativa de Layout</h1>
              <div>
                <h5>10/03/2019</h5>
              </div>
              <div>
                <label> Primeira postagem</label>
              </div>
              <div>
                <a href="#comentarios" class=""> Comentários </a>
              </div>
            </div>
          </div>

        </div>
          <div class="footer text-center">
            <a href="#"> Página anterior </a>
            <a href="#"> Página seguinte </a>

            <div class="footer-content" style="background-color: #448ccb; width:auto; height:25px;">
              Copyright - Fazenda Haua
            </div>
          </div>


          <!--Modal login-->
          <div class="modal fade" id="loginModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content modal-top-border">
                  <div class="modal-title-border">
                      <div>
                        <button type="button" class="close modal-close-button" data-dismiss="modal" aria-label="Fechar">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="text-center">
                        <h2 class="banner-content modal-title"> LOGIN </h2>
                      </div>
                  </div>
                  <div class="modal-menu">
                    <div>
                      <a href="" class="modal-cadastrar" data-toggle="modal" data-target="#cadastrarModal" data-dismiss="modal"> Cadastrar </a>
                      <a href="" class="modal-esqueciSenha" data-toggle="modal" data-target="#esqueciSenhaModal" data-dismiss="modal"> Esqueci a senha </a>
                    </div>
                  </div>
                  <div class="text-center modal-content">
                    <form class="form-group modal-form" action="" method="POST">
                        <div class="form-group row modal-input">
                          <label> Nome: </label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="loginUsername"/>
                          </div>
                        </div>
                        <div class="form-group row modal-input">
                          <label> Senha: </label>
                          <div class="col-sm-9">
                            <input type="password" class="form-control" name="loginPassword"/>
                          </div>
                        </div>
                        <input type="submit" class="btn modal-submit-button btn-primary" value="Entrar" name="loginButton"/>
                    </form>
                  </div>
                </div>
            </div>
          </div>
          <!--Modal cadastrar-->
          <div class="modal fade" id="cadastrarModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content modal-top-border">
                  <div class="modal-title-border">
                      <div>
                        <button type="button" class="close modal-close-button" data-dismiss="modal" aria-label="Fechar">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="text-center">
                        <h2 class="banner-content modal-title"> CADASTRO </h2>
                      </div>
                  </div>
                  <div class="modal-menu">
                    <div>
                      <a href="" class="modal-cadastrar" data-toggle="modal" data-target="#loginModal" data-dismiss="modal"> Login </a>
                      <a href="" class="modal-esqueciSenha" data-toggle="modal" data-target="#esqueciSenhaModal" data-dismiss="modal"> Esqueci a senha </a>
                    </div>
                  </div>
                  <div class="text-center modal-content">
                    <form class="form-group modal-form" action="" method="POST">
                        <div class="form-group row modal-input">
                          <label> Nome: </label>
                          <div class="col-sm-4">
                            <input type="text" class="form-control" name="cadastrarUsername"/>
                          </div>
                          <label> E-mail: </label>
                          <div class="col-sm-4">
                            <input type="text" class="form-control" name="cadastrarEmail"/>
                          </div>
                        </div>
                        <div class="form-group row modal-input">
                          <label> Senha: </label>
                          <div class="col-sm-4">
                            <input type="password" class="form-control" name="cadastrarPassword"/>
                          </div>
                          <label> Confirmar senha: </label>
                          <div class="col-sm-4">
                            <input type="password" class="form-control" name="cadastrarConf-password"/>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <input type="submit" class="btn modal-submit-button btn-primary" value="Cadastrar" name="cadastrarButton"/>
                        </div>
                    </form>
                  </div>
                </div>
            </div>
          </div>
          <!--Modal esqueciSenha-->
          <div class="modal fade" id="esqueciSenhaModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content modal-top-border">
                  <div class="modal-title-border">
                      <div>
                        <button type="button" class="close modal-close-button" data-dismiss="modal" aria-label="Fechar">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="text-center">
                        <h2 class="banner-content modal-title"> RECUPERAÇÃO DE SENHA </h2>
                      </div>
                  </div>
                  <div class="modal-menu">
                    <div>
                      <a href="" class="modal-cadastrar" data-toggle="modal" data-target="#cadastrarModal" data-dismiss="modal"> Cadastrar </a>
                      <a href="" class="modal-esqueciSenha" data-toggle="modal" data-target="#loginModal" data-dismiss="modal"> Login </a>
                    </div>
                  </div>
                  <div class="text-center modal-content">
                    <form class="form-group modal-form" action="" method="POST">
                        <label> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</label>
                        <div class="form-group row modal-input">
                          <label> E-mail: </label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="esqueciSenhaEmail"/>
                          </div>
                        </div>
                        <input type="submit" class="btn modal-submit-button btn-primary" value="Recuperar senha" name="esqueciSenhaButton"/>
                    </form>
                  </div>
                </div>
            </div>
          </div>
    </body>
</html>
